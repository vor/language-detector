FROM ubuntu:16.04
MAINTAINER Ryan Vo "ryan_vo@symantec.com"

# Install tools for container.
RUN apt-get update \
  # Install cld2.
  && apt-get install -y git build-essential python-dev \
  && cd ~ \
  && git clone https://github.com/CLD2Owners/cld2.git \
  && cd cld2/internal \
  && ./compile_libs.sh \
  && cp libcld2.so /usr/lib \
  && cp libcld2_full.so /usr/lib \
  && cd ~ \
  && git clone https://github.com/mikemccand/chromium-compact-language-detector.git \
  && cd chromium-compact-language-detector \
  && python setup.py build \
  && python setup_full.py build \
  && python setup.py install \
  && python setup_full.py install \
  && cd ~ \
  && apt-get purge -y git build-essential python-dev \
  && rm -rf ~/cld2 \
  && rm -rf ~/chromium-compact-language-detector \
  # Install gunicorn.
  && apt-get install -y python python-pip \
  && pip install --upgrade pip \
  && pip install gunicorn \
  # Clean up.
  && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY *py ./

ENV WEB_CONCURRENCY 2
EXPOSE 5000
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "api:app"]