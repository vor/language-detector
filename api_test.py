# -*- coding: utf-8 -*-
import api
import json
import unittest
import urllib

class ApiTestCase(unittest.TestCase):
  """Tests 'api.py'."""

  def setUp(self):
    api.app.config['TESTING'] = True
    self.app = api.app.test_client()

  def tearDown(self):
    pass

  def test_en(self):
    """Tests if API can detect English."""
    query = 'Our best protection for any way you choose to connect'
    rv = self.app.get('/detections?q=' + urllib.quote(query))
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['isReliable'] == True
    assert data['languages'][0]['languageCode'] == 'en'
    assert data['languages'][0]['confidence'] > 0

  def test_fr(self):
    """Tests if API can detect French."""
    query = 'Protégez activement votre environnement numérique.'
    rv = self.app.get('/detections?q=' + urllib.quote(query))
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['isReliable'] == True
    assert data['languages'][0]['languageCode'] == 'fr'
    assert data['languages'][0]['confidence'] > 0

  def test_jp(self):
    """Tests if API can detect Japanese."""
    query = 'インターネットを思いっきり楽しもう！'
    rv = self.app.get('/detections?q=' + urllib.quote(query))
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['isReliable'] == True
    assert data['languages'][0]['languageCode'] == 'ja'
    assert data['languages'][0]['confidence'] > 0

  def test_unknown(self):
    """Tests if API responds with unknown for empty query."""
    rv = self.app.get('/detections?q=')
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert data['isReliable'] == False
    assert data['languages'][0]['languageCode'] == 'un'

  def test_400(self):
    """Tests if API responds with 400 Bad Request for missing query."""
    rv = self.app.get('/detections')
    assert rv.status_code == 400

  def test_404(self):
    """Tests if API responds with 404 for unknown routes."""
    rv = self.app.get('/blah')
    assert rv.status_code == 404

if __name__ == '__main__':
  unittest.main()