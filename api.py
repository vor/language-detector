import cld2full
import flask

app = flask.Flask(__name__)

@app.route('/detections')
def getDetections():
  """
  Processes a string and returns language detection information.
  q -- the string to process
  """
  query = flask.request.args.get('q')
  if query == None:
    return 'Missing query q.', 400
  query = query.encode('utf-8')
  detection = cld2full.detect(query)
  response = {
    'isReliable': detection[0],
    'textBytes': detection[1],
    'languages': [{
      'language': detail[0],
      'languageCode': detail[1],
      'confidence': detail[2],
      'score': detail[3]
    } for detail in detection[2]]
  }
  return flask.jsonify(response)

if __name__ == '__main__':
  app.run(host='0.0.0.0')